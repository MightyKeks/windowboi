TrayTip, WindowBoi, Press Alt + F1 for Info on WindowBoi

!e::
	WinGetTitle, Title, A
	WinSet, AlwaysOnTop, On, %Title%
	RegExMatch(Title, " - AlwaysOnTop", NewTitle)
	if (not NewTitle = "")
	{
		return
	}
	WinSetTitle, %Title%,, %Title% - AlwaysOnTop
	return

!q::
	WinGetTitle, Title, A
	WinSet, AlwaysOnTop, Off, %Title%
	RegExMatch(Title, ".+(?= - AlwaysOnTop)", NewTitle)
	if (NewTitle = "")
	{
		return
	}
	WinSetTitle, %Title%,, %NewTitle%
	return

!m::
	Run "C:\Windows\System32\SndVol.exe"
	return

!F1::
	Gui, GuiName:New,,WindowBoi
	Gui, Font, s15
	Gui, Add, Text,, Alt + Q: Disable AlwaysOnTop
	Gui, Add, Text,, Alt + E: Enable AlwaysOnTop
	Gui, Add, Text,, Alt + M: Open Volume Mixer
	Gui, Show, w350 h140 Center
	WinWait, WindowBoi
	Send, !e